#!/bin/bash

TRACES=$(ls trace*.txt)

for TRACE in $TRACES
do
    echo "Running trace $TRACE"

    # Get reference and working output
    ./sdriver.pl -t $TRACE -s ./tsh -a "-p" > tsh-out
    ./sdriver.pl -t $TRACE -s ./tshref -a "-p" > tshref-out

    # Clean up the results so the diff is useful
    # First, change all instances of tshref to tsh (for ps)
    sed -i 's/tshref/tsh/g' tshref-out

    # Next, clean PIDs out of PS output
    sed -i 's/[0-9]\+ pts/PID pts/g' tsh-out
    sed -i 's/[0-9]\+ pts/PID pts/g' tshref-out

    # Last, clean PIDs out of job-related output from the shell
    sed -i 's/([0-9]\+)/(PID)/g' tsh-out
    sed -i 's/([0-9]\+)/(PID)/g' tshref-out

    # And diff the results
    diff tsh-out tshref-out
done

# Clean up dangling files
rm tsh-out tshref-out

echo "Done!"
