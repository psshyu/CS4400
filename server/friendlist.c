/*
 * friendlist.c - [Starting code for] a web-based friend-graph manager.
 *
 * Based on:
 *  tiny.c - A simple, iterative HTTP/1.0 Web server that uses the 
 *      GET method to serve static and dynamic content.
 *   Tiny Web server
 *   Dave O'Hallaron
 *   Carnegie Mellon University
 */
#include "csapp.h"
#include "dictionary.h"
#include "more_string.h"

static void doit(int fd);
static dictionary_t *read_requesthdrs(rio_t *rp);
static void read_postquery(rio_t *rp, dictionary_t *headers, dictionary_t *d);
static void clienterror(int fd, char *cause, char *errnum, 
                        char *shortmsg, char *longmsg);
static void print_stringdictionary(dictionary_t *d);
static void serve_request(int fd, dictionary_t *query);
static void serve_befriend(int fd, dictionary_t *query, dictionary_t *friends);
static void serve_friends(int fd, dictionary_t *query, dictionary_t *friends);
static void serve_unfriend(int fd, dictionary_t *query, dictionary_t *friends);
static void serve_introduce(int fd, dictionary_t *query, dictionary_t *friends);
static void* serve_client_thread(void* args);
static dictionary_t* friends;
static char* self_host;
static char* self_port;
int main(int argc, char **argv) 
{
  int listenfd, connfd;
  //dictionary_t* friends;
  char hostname[MAXLINE], port[MAXLINE];
  socklen_t clientlen;
  struct sockaddr_storage clientaddr;

  /* Check command line args */
  if (argc != 2) {
    fprintf(stderr, "usage: %s <port>\n", argv[0]);
    exit(1);
  }
  self_port = argv[1];
  listenfd = Open_listenfd(argv[1]);

  exit_on_error(0);
  /* Also, don't stop on broken connections: */
  Signal(SIGPIPE, SIG_IGN);
  friends = make_dictionary(COMPARE_CASE_SENS, NULL);
  while (1) {
    pthread_t tid;
    
    clientlen = sizeof(clientaddr);
    connfd = Accept(listenfd, (SA *)&clientaddr, &clientlen);
    
    int* targs = malloc(sizeof(int));
    *targs = connfd;

    if (connfd >= 0) {
      Getnameinfo((SA *) &clientaddr, clientlen, hostname, MAXLINE, 
                  port, MAXLINE, 0);
      printf("Accepted connection from (%s, %s)\n", hostname, port);
      pthread_create(&tid, NULL, serve_client_thread, targs);
      pthread_detach(tid);
    }
  }
  exit(0);
}
void* serve_client_thread(void* args){
  doit(*(int*)args);
  free(args);
}

/*
 * doit - handle one HTTP request/response transaction
 */
void doit(int fd) 
{
  printf("*** in doit() ***\n");
  char buf[MAXLINE], *method, *uri, *version;
  rio_t rio;
  dictionary_t *headers, *query;

  /* Read request line and headers */
  Rio_readinitb(&rio, fd);
  if (Rio_readlineb(&rio, buf, MAXLINE) <= 0)
    return;
  
  if (!parse_request_line(buf, &method, &uri, &version)) {
    printf("buf: %s\n", buf);
    printf("version: %s\n", version);
    printf("method: %s\n", method);
    printf("PARSING URI: %s\n", uri);
    clienterror(fd, method, "400", "Bad Request",
                "Friendlist did not recognize the request");
  } 
  else {
    if (strcasecmp(version, "HTTP/1.0")
        && strcasecmp(version, "HTTP/1.1")) {
      clienterror(fd, version, "501", "Not Implemented",
                  "Friendlist does not implement that version");
    } else if (strcasecmp(method, "GET")
               && strcasecmp(method, "POST")) {
      clienterror(fd, method, "501", "Not Implemented",
                  "Friendlist does not implement that method");
    } else {
      headers = read_requesthdrs(&rio);

      /* Parse all query arguments into a dictionary */
      query = make_dictionary(COMPARE_CASE_SENS, free);
      parse_uriquery(uri, query);
      if (!strcasecmp(method, "POST"))
        read_postquery(&rio, headers, query);

      /* For debugging, print the dictionary */
      print_stringdictionary(query);

      if (starts_with("/befriend", uri))
      {
        serve_befriend(fd, query, friends);
      }
      else if (starts_with("/friends", uri)){
	      serve_friends(fd, query, friends);
        printf("finding your friends!\n");
      }
      else if (starts_with("/unfriend", uri)){
	        serve_unfriend(fd, query, friends);
      }
      else if (starts_with("/introduce", uri)){
	      serve_introduce(fd, query, friends);
      }
      else    
        serve_request(fd, query);

      /* Clean up */
      free_dictionary(query);
      free_dictionary(headers);
    }

    /* Clean up status line */
    free(method);
    free(uri);
    free(version);
  }
  Close(fd);
}

/*
 * read_requesthdrs - read HTTP request headers
 */
dictionary_t *read_requesthdrs(rio_t *rp) 
{
  char buf[MAXLINE];
  dictionary_t *d = make_dictionary(COMPARE_CASE_INSENS, free);

  Rio_readlineb(rp, buf, MAXLINE);
  printf("%s", buf);
  while(strcmp(buf, "\r\n")) {
    Rio_readlineb(rp, buf, MAXLINE);
    printf("%s", buf);
    parse_header_line(buf, d);
  }
  
  return d;
}

void read_postquery(rio_t *rp, dictionary_t *headers, dictionary_t *dest)
{
  char *len_str, *type, *buffer;
  int len;
  
  len_str = dictionary_get(headers, "Content-Length");
  len = (len_str ? atoi(len_str) : 0);

  type = dictionary_get(headers, "Content-Type");
  
  buffer = malloc(len+1);
  Rio_readnb(rp, buffer, len);
  buffer[len] = 0;

  if (!strcasecmp(type, "application/x-www-form-urlencoded")) {
    parse_query(buffer, dest);
  }

  free(buffer);
}

static char *ok_header(size_t len, const char *content_type) {
  char *len_str, *header;
  
  header = append_strings("HTTP/1.0 200 OK\r\n",
                          "Server: Friendlist Web Server\r\n",
                          "Connection: close\r\n",
                          "Content-length: ", len_str = to_string(len), "\r\n",
                          "Content-type: ", content_type, "\r\n\r\n",
                          NULL);
  free(len_str);

  return header;
}

/*
 * serve_request - example request handler
 */
static void serve_request(int fd, dictionary_t *query)
{
  size_t len;
  char *body, *header;

  body = strdup("alice\nbob");

  len = strlen(body);

  /* Send response headers to client */
  header = ok_header(len, "text/html; charset=utf-8");
  Rio_writen(fd, header, strlen(header));
  printf("Response headers:\n");
  printf("%s", header);

  free(header);

  /* Send response body to client */
  Rio_writen(fd, body, len);

  free(body);
}

/*
 * introduce - example request handler
 */
static void serve_introduce(int fd, dictionary_t *query, dictionary_t *friends)
{
  size_t len;
  dictionary_t *headers, *currFriends, *usrFriends;
  char *body, *header, *usr, *friend, *host, *port, *newURI, *buf[MAXLINE], *usrFrdList;
  char **returned;
  usr = dictionary_get(query, "user");
  friend = dictionary_get(query, "friend");
  host = dictionary_get(query, "host");
  port = dictionary_get(query, "port");

  if (dictionary_get(friends, usr) == NULL)
    usrFriends = make_dictionary(COMPARE_CASE_INSENS, NULL); //alloc new friend dict
  else{
    printf("getting usr's friends:\n");
    usrFriends = dictionary_get(friends, usr);
  }

  int clientfd = Open_clientfd(host, port);
  rio_t rio;
  Rio_readinitb(&rio, clientfd);
  newURI = append_strings("GET /friends?user=", friend , " HTTP/1.1\r\n\r\n", NULL);
  size_t len_newURI = strlen(newURI);

  Rio_writen(clientfd, newURI, len_newURI);

  if(Rio_readlineb(&rio, buf, MAXLINE)<=0){
    return;
  }
  headers = read_requesthdrs(&rio);
  char *advBytes = dictionary_get(headers, "Content-length");
  printf("content length: %s\n", advBytes);
  Rio_readnb(&rio, buf, *advBytes);
  //Rio_readlineb(&rio, newURI, MAXLINE);
  //printf("**** returned buff: %s **** \n", buf);
  returned = split_string(buf, '\n');
  int i = 0; 

  while(!starts_with("00", returned[i]))
  {
    printf("return loop: %s \n", returned[i]);

      if(dictionary_get(friends, returned[i]) == NULL) {
        currFriends = make_dictionary(COMPARE_CASE_INSENS, NULL);
      }
      else {
        currFriends = dictionary_get(friends, returned[i]);
      }

      dictionary_set(currFriends, usr, NULL);
      dictionary_set(friends, returned[i], currFriends);
      dictionary_set(usrFriends, returned[i], NULL);
      printf("%s's friends: \n", returned[i]);
      print_stringdictionary(currFriends);
      i++;
    
  }

  dictionary_set(friends, usr, usrFriends);
  printf("friends dict: \n");
  print_stringdictionary(friends);
  printf("user friends: \n");
  print_stringdictionary(usrFriends);
  usrFrdList = join_strings(dictionary_keys(usrFriends), '\n');

  body = strdup(usrFrdList);
  len = strlen(body);



  /* Send response headers to client */
  header = ok_header(len, "text/html; charset=utf-8");
  Rio_writen(fd, header, strlen(header));
  printf("Response headers:\n");
  printf("%s", header);

  free(header);

  /* Send response body to client */
  Rio_writen(fd, body, len);

  free(body);
  //Close(clientfd);
}

/*
 * serve_friends - example request handler
 */
static void serve_friends(int fd, dictionary_t *query, dictionary_t *friends)
 {
   size_t len;
   char *body, *header, *usr, *usrFrdList;
   dictionary_t *usrFriends;
   usr = dictionary_get(query, "user");

   if ((usrFriends = dictionary_get(friends, usr)) != NULL){
     usrFrdList = join_strings(dictionary_keys(usrFriends), '\n');
   }
   else{
     printf("%s not in friends dictionary!\n", usr);
     usrFrdList = "";
   }
   body = strdup(usrFrdList);

   len = strlen(body);

   /* Send response headers to client */
   header = ok_header(len, "text/html; charset=utf-8");
   Rio_writen(fd, header, strlen(header));
   printf("Response headers:\n");
   printf("%s", header);

   free(header);

   /* Send response body to client */
   Rio_writen(fd, body, len);

   free(body);
 }



/*
 * serve_befriend - example request handler
 */
static void serve_befriend(int fd, dictionary_t *query, dictionary_t *friends)
{
  size_t len;
  char *body, *header, *usr, *frd, *usrFrdList;
  char **frdList;
  dictionary_t *usrFriends, *currFriends;

  usr = dictionary_get(query, "user");
  frd = dictionary_get(query, "friends");

  print_stringdictionary(query);

  if (dictionary_get(friends, usr) == NULL)
    usrFriends = make_dictionary(COMPARE_CASE_INSENS, NULL); //alloc new friend dict
  else{
    printf("getting usr's friends:\n");
    usrFriends = dictionary_get(friends, usr);
  }

  printf("top\n");
  print_stringdictionary(usrFriends); 
  frdList = split_string(frd, '\n');
  int i = 0;
  while(frdList[i] != NULL)
  {
    printf("befriending %s\n", frdList[i]);
    if(dictionary_get(friends, frdList[i]) == NULL) {
      currFriends = make_dictionary(COMPARE_CASE_INSENS, NULL);
    }
    else {
      currFriends = dictionary_get(friends, frdList[i]);
    }

    dictionary_set(currFriends, usr, NULL);
    dictionary_set(friends, frdList[i], currFriends);
    dictionary_set(usrFriends, frdList[i], NULL);
    printf("%s's friends: \n", frdList[i]);
    print_stringdictionary(currFriends);
    i++;
  }

  dictionary_set(friends, usr, usrFriends);
  printf("friends dict: \n");
  print_stringdictionary(friends);
  printf("user friends: \n");
  print_stringdictionary(usrFriends);
  usrFrdList = join_strings(dictionary_keys(usrFriends), '\n');

  body = strdup(usrFrdList);
  len = strlen(body);

  /* Send response headers to client */
  header = ok_header(len, "text/html; charset=utf-8");
  Rio_writen(fd, header, strlen(header));
  printf("Response headers:\n");
  printf("%s", header);

  free(header);
  /* Send response body to client */
  Rio_writen(fd, body, len);
  free(body);
}

/*
 * serve_friends - example request handler
 */
static void serve_unfriend(int fd, dictionary_t *query, dictionary_t *friends)
{
  size_t len;
  char *body, *header, *usr, *frd, *usrFrdList;
  char **frdList;
  dictionary_t *usrFriends, *currFriends;

  usr = dictionary_get(query, "user");
  frd = dictionary_get(query, "friends");

  print_stringdictionary(query);

  if (dictionary_get(friends, usr) == NULL)
    usrFriends = make_dictionary(COMPARE_CASE_INSENS, NULL); 
  else{
    printf("getting usr's friends:\n");
    usrFriends = dictionary_get(friends, usr);
  }

  print_stringdictionary(usrFriends);
  frdList = split_string(frd, '\n');
  int i = 0;
  while(frdList[i] != NULL){
    printf("unfriending %s\n", frdList[i]);
    currFriends = dictionary_get(friends, frdList[i]);
    dictionary_remove(currFriends, usr);
    dictionary_remove(usrFriends, frdList[i]);
    i++;
  }
  usrFrdList = join_strings(dictionary_keys(usrFriends), '\n');
  body = strdup(usrFrdList);

  len = strlen(body);

  /* Send response headers to client */
  header = ok_header(len, "text/html; charset=utf-8");
  Rio_writen(fd, header, strlen(header));
  printf("Response headers:\n");
  printf("%s", header);

  free(header);

  /* Send response body to client */
  Rio_writen(fd, body, len);

  free(body);
}



/*
 * clienterror - returns an error message to the client
 */
void clienterror(int fd, char *cause, char *errnum, 
		 char *shortmsg, char *longmsg) 
{
  size_t len;
  char *header, *body, *len_str;

  body = append_strings("<html><title>Friendlist Error</title>",
                        "<body bgcolor=""ffffff"">\r\n",
                        errnum, " ", shortmsg,
                        "<p>", longmsg, ": ", cause,
                        "<hr><em>Friendlist Server</em>\r\n",
                        NULL);
  len = strlen(body);

  /* Print the HTTP response */
  header = append_strings("HTTP/1.0 ", errnum, " ", shortmsg, "\r\n",
                          "Content-type: text/html; charset=utf-8\r\n",
                          "Content-length: ", len_str = to_string(len), "\r\n\r\n",
                          NULL);
  free(len_str);
  
  Rio_writen(fd, header, strlen(header));
  Rio_writen(fd, body, len);

  free(header);
  free(body);
}

static void print_stringdictionary(dictionary_t *d)
{
  int i, count;

  count = dictionary_count(d);
  for (i = 0; i < count; i++) {
    printf("%s=%s\n",
           dictionary_key(d, i),
           (const char *)dictionary_value(d, i));
  }
  printf("\n");
}
